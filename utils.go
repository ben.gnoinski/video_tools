package main

import (
    "strings"
)

func StringInSplice (name string, splice []string) bool {
    for _, s := range splice {
        if name == s {
            return true
        }
    }
    return false
}

func SpliceContainsString (name string, splice []string) bool {
    for _, s := range splice {
        if strings.Contains(s, name) {
            return true
        }
    }
    return false
}
