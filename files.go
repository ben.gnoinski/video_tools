package main

import (
    "fmt"
    "io"
    "io/fs"
    "os"
    "path/filepath"
    "strings"
    "strconv"
    "time"
)

const localWorking string = "/mnt/capri_videos/working/"
const media string = "/media/ben"

type fileCopy struct {
    dest string
    mtime time.Time
    source string
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func getFiles(dir string, fileExtension string) []string {
    files := []string {}
 
    err := filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
        if err != nil {
            fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
            return err
        }

        if info.IsDir() {
            return nil
        }

        if fileExtension != "" {
            if strings.Contains(info.Name(), fileExtension) {
                files = append(files, path)
            }
        } else {
            files = append(files, path)
        }

        return nil
    })
    if err != nil {
        fmt.Printf("error walking the path %q: %v\n", dir, err)
        return nil
    }

    return files
}

func copyFile(src string, dst string, mtime time.Time, atime time.Time, dryRun bool) {

    if ! dryRun { 
        source, err := os.Open(src)
        check(err)
        defer source.Close()
    
        dest, err := os.Create(dst)
        check(err)
        defer dest.Close()
    
        fmt.Println("Starting to copy " + src + "to " + dst)
        _, err = io.Copy(dest, source)
        check(err)
    
        err = os.Chtimes(dst, atime, mtime)
        check(err)
    } else {
        fmt.Println("Would have copied " + src + " to " + dst)
    }
}

func getDirs(dir string) []string {
    dirs := []string {}
 
    err := filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
        if err != nil {
            fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
            return err
        }

        if info.IsDir() {
            dirs = append(dirs, path)
            return nil
        }
        return nil
    })

    if err != nil {
        fmt.Printf("error walking the path %q: %v\n", dir, err)
        return nil
    }
    return dirs
}

func processGoProVideos(files []string, workingDirs []string, existingFiles []string, dryRun bool) {
    fmt.Println("starting copy")
    filesToCopy := []fileCopy {}
    yearsWeeks := []string {}
    for _, file := range files {
        video, err := os.Stat(file)
        check(err)
        videoSize := video.Size()

        fileName := video.Name()
        modTime := video.ModTime()
        year, week := modTime.ISOWeek()
        currentyw := strconv.Itoa(year) + "_week_" + strconv.Itoa(week)
        dest := localWorking + currentyw + "/" + fileName

        if ! StringInSplice(currentyw, yearsWeeks) {
            yearsWeeks = append(yearsWeeks, currentyw)
        }

        if ! SpliceContainsString(fileName, existingFiles) {
            filesToCopy = append(filesToCopy, fileCopy{dest: dest, mtime: modTime, source: file})
            check(err)
        } else {
            existingVideo, err := os.Stat(dest)
            check(err)
            existingSize := existingVideo.Size()

            if existingSize != videoSize {
                fmt.Println(file + " already exists on disk but size does not match")
                fmt.Println(dest)
                fmt.Println("Sizes between files do not match")
                fmt.Printf("Existing size %d", existingSize)
	        fmt.Printf("New video size %d", videoSize)
            } else {
                fmt.Println(file + " already exists on disk")
            }
        }
    }

    for _, yw := range yearsWeeks {
        if ! SpliceContainsString(yw, workingDirs) {
            create := localWorking + yw
            fmt.Println("folder does not exist creating " + create)
            dirPerms := os.FileMode(0755)
            if dryRun {
                fmt.Println("Would have created folder " + create)
            } else {
                os.Mkdir(create, dirPerms)
            }
        }
    }

    for _, file := range filesToCopy {
    
        mtime := file.mtime
        atime := file.mtime

        copyFile(file.source, file.dest, mtime, atime, dryRun)
    }
}

func goproWorking(dryRun bool) {
    existingFiles := getFiles(localWorking, "MP4")
    goproFiles := getFiles(media, "MP4")

    workingDirs := getDirs(localWorking)

    // Need to re-order the gopro files list so that when we copy the files the create/modify timestamps are more amenable to sorting
    files := []string {}
    for _, file := range goproFiles {
        if strings.Contains(file, "GX01") {
            files = append(files, file)
        }
    }

    orderedGoproFiles := []string {}

    for _, file := range files {
        search := file[len(file)-8:]
        for _, chapter := range goproFiles {
            if strings.Contains(chapter, search) {
                orderedGoproFiles = append(orderedGoproFiles, chapter)
            }
        }
    }

    processGoProVideos(orderedGoproFiles, workingDirs, existingFiles, dryRun)
}

func main() {

    dryRun := false

    if StringInSplice("--dryrun", os.Args[:]) {
        dryRun = true
    }

    switch arg := os.Args[1]; arg {
    case "working":
        fmt.Println("starting working")
        goproWorking(dryRun)
    }
}
